# Steam Shortcuts IO

## Description
A library for writing and reading the shortcuts.vdf file where Steam saves shortcuts.

## Disclaimer
This is a work in progress and doesn't contain any tests! If your shortcuts file gets destroyed, corrupted or is changed in any way that you didn't want, that's on you!

## Usage

### Add Maven registry
```xml
<repositories>
  <repository>
    <id>gitlab-maven</id>
    <url>https://gitlab.com/api/v4/projects/21859334/packages/maven</url>
  </repository>
</repositories>
```

### Add dependency
```xml
<dependency>
    <groupId>se.kodaren.steamshortcutsio</groupId>
    <artifactId>steam-shortcuts-io</artifactId>
    <version>1.0.0</version>
</dependency>
```

### Example
```java
VdfShortcutsWriter writer = new VdfShortcutsWriter();

VdfShortcut baldursgate3 = new VdfShortcut();
baldursgate3.setKey("0");
baldursgate3.setAppName("Baldur's Gate 3");
baldursgate3.setExe("\"lutris\"");
baldursgate3.setLaunchOptions("lutris:rungame/baldurs-gate-3");
baldursgate3.setShortcutPath("/usr/share/applications/net.lutris.Lutris.desktop");
baldursgate3.setStartDir("\"./\"");

baldursgate3.getTags().add(new VdfTag("favorite"));

List<VdfShortcut> shortcuts = new ArrayList<>();
shortcuts.add(baldursgate3);

writer.create(new File("shortcuts.vdf"), shortcuts);
```