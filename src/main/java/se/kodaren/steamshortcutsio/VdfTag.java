package se.kodaren.steamshortcutsio;

/**
 * Model of a tag in a shortcut.
 */
public class VdfTag {
    private String value;

    public VdfTag(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
}
