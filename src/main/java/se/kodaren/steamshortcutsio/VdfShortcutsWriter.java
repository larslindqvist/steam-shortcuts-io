package se.kodaren.steamshortcutsio;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.lang.reflect.Field;
import java.nio.charset.StandardCharsets;
import java.util.List;

/**
 * A writer for VDF shortcuts.
 */
public class VdfShortcutsWriter {
    private static final String SHORTCUTS = "shortcuts";
    private static final String ALLOW = "Allow";
    private static final int NUL = 0;
    private static final int SOH = 1;
    private static final int STX = 2;
    private static final int BS = 8;

    /**
     * Creates a new file and writes shortcuts information to it.
     *
     * @param file A non-existing file to create and write to.
     * @param vdfShortcuts Shortcuts to write.
     * @throws IOException If shortcuts can't be writted to the file.
     */
    public void create(File file, List<VdfShortcut> vdfShortcuts) throws IOException {
        if(file.exists()) {
            throw new IOException("File '" + file.getAbsolutePath() + "' already exists.");
        }

        final FileOutputStream fos = new FileOutputStream(file);

        writeStart(fos);

        // Write entry
        for(VdfShortcut shortcut : vdfShortcuts) {
            writeEntry(fos, shortcut);
        }

        writeEnd(fos);
    }

    private void writeStart(FileOutputStream fileOutputStream) throws IOException {
        fileOutputStream.write(NUL);
        fileOutputStream.write(SHORTCUTS.getBytes(StandardCharsets.US_ASCII));
        fileOutputStream.write(NUL);
    }

    private void writeEnd(FileOutputStream fileOutputStream) throws IOException {
        fileOutputStream.write(BS);
        fileOutputStream.write(BS);
    }

    private void writeEntry(FileOutputStream fos, VdfShortcut shortcut) throws IOException {
        fos.write(NUL);
        fos.write(shortcut.getKey().getBytes(StandardCharsets.US_ASCII));
        fos.write(NUL);

        Field[] fields = VdfShortcut.class.getDeclaredFields();
        for(Field f : fields) {
            Object o;
            try {
                o = f.get(shortcut);
            } catch (IllegalAccessException e) {
                throw new RuntimeException(e);
            }

            if(o instanceof String) {
                String s = (String)o;

                if (f.isAnnotationPresent(VdfField.class)) {
                    VdfField vdfField = f.getAnnotation(VdfField.class);
                    String value = vdfField.value();
                    VdfFieldType type = vdfField.type();

                    if(type == VdfFieldType.SOH) {
                        fos.write(SOH);
                        fos.write(value.getBytes(StandardCharsets.US_ASCII));
                        fos.write(NUL);
                        fos.write(s.getBytes(StandardCharsets.US_ASCII));
                        fos.write(NUL);
                    }
                    else if(type == VdfFieldType.STX) {
                        fos.write(STX);
                        fos.write(value.getBytes(StandardCharsets.US_ASCII));
                        fos.write(NUL);

                        if(value.contains(ALLOW)) {
                            fos.write(SOH);
                        }
                        else {
                            fos.write(NUL);
                        }
                        fos.write(NUL);
                        fos.write(NUL);

                        fos.write(NUL);
                    }
                }
            }
            else if (o instanceof List) {
                @SuppressWarnings("unchecked")
                List<VdfTag> list = (List<VdfTag>)o;

                if (f.isAnnotationPresent(VdfField.class)) {
                    VdfField vdfField = f.getAnnotation(VdfField.class);
                    String value = vdfField.value();

                    fos.write(NUL);
                    fos.write(value.getBytes(StandardCharsets.US_ASCII));
                    fos.write(NUL);

                    for (int i = 0; i < list.size(); i++) {
                        fos.write(SOH);
                        fos.write(Integer.toString(i).getBytes(StandardCharsets.US_ASCII));
                        fos.write(NUL);
                        fos.write(list.get(i).getValue().getBytes(StandardCharsets.US_ASCII));
                        fos.write(NUL);
                    }

                    fos.write(BS);
                }
            }
        }

        fos.write(BS);
    }
}
