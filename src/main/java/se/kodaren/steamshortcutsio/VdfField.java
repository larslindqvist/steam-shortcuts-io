package se.kodaren.steamshortcutsio;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

@Retention(RetentionPolicy.RUNTIME)
@interface VdfField {
    String value();
    VdfFieldType type() default VdfFieldType.SOH;
}
