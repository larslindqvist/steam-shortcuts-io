package se.kodaren.steamshortcutsio;

import java.util.ArrayList;
import java.util.List;

/**
 * Model of a shortcut.
 */
public class VdfShortcut {
    String key = "";
    @VdfField("appname")
    String appName = "";
    @VdfField("Exe")
    String exe = "";
    @VdfField("StartDir")
    String startDir = "";
    @VdfField("icon")
    String icon = "";
    @VdfField("ShortcutPath")
    String shortcutPath = "";
    @VdfField("LaunchOptions")
    String launchOptions = "";
    @VdfField(value = "IsHidden", type = VdfFieldType.STX)
    String isHidden = "";
    @VdfField(value = "AllowDesktopConfig", type = VdfFieldType.STX)
    String allowDesktopConfig = "";
    @VdfField(value = "AllowOverlay", type = VdfFieldType.STX)
    String allowOverlay = "";
    @VdfField(value = "OpenVR", type = VdfFieldType.STX)
    String openVR = "";
    @VdfField(value = "Devkit", type = VdfFieldType.STX)
    String devkit = "";
    @VdfField("DevkitGameID")
    String devkitGameID = "";
    @VdfField(value = "LastPlayTime", type = VdfFieldType.STX)
    String lastPlayTime = "";
    @VdfField("tags")
    List<VdfTag> tags = new ArrayList<>();

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getAppName() {
        return this.appName;
    }

    public String getExe() {
        return this.exe;
    }

    public String getStartDir() {
        return this.startDir;
    }

    public String getIcon() {
        return this.icon;
    }

    public String getShortcutPath() {
        return this.shortcutPath;
    }

    public String getLaunchOptions() {
        return this.launchOptions;
    }

    public String getIsHidden() {
        return this.isHidden;
    }

    public String getAllowDesktopConfig() {
        return this.allowDesktopConfig;
    }

    public String getAllowOverlay() {
        return this.allowOverlay;
    }

    public String getOpenVR() {
        return this.openVR;
    }

    public String getDevkit() {
        return this.devkit;
    }

    public String getDevkitGameID() {
        return this.devkitGameID;
    }

    public String getLastPlayTime() {
        return this.lastPlayTime;
    }

    public List<VdfTag> getTags() {
        return this.tags;
    }

    public void setAppName(String appName) {
        this.appName = appName;
    }

    public void setExe(String exe) {
        this.exe = exe;
    }

    public void setStartDir(String startDir) {
        this.startDir = startDir;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    public void setShortcutPath(String shortcutPath) {
        this.shortcutPath = shortcutPath;
    }

    public void setLaunchOptions(String launchOptions) {
        this.launchOptions = launchOptions;
    }

    public void setIsHidden(String isHidden) {
        this.isHidden = isHidden;
    }

    public void setAllowDesktopConfig(String allowDesktopConfig) {
        this.allowDesktopConfig = allowDesktopConfig;
    }

    public void setAllowOverlay(String allowOverlay) {
        this.allowOverlay = allowOverlay;
    }

    public void setOpenVR(String openVR) {
        this.openVR = openVR;
    }

    public void setDevkit(String devkit) {
        this.devkit = devkit;
    }

    public void setDevkitGameID(String devkitGameID) {
        this.devkitGameID = devkitGameID;
    }

    public void setLastPlayTime(String lastPlayTime) {
        this.lastPlayTime = lastPlayTime;
    }

    public void setTags(List<VdfTag> tags) {
        this.tags = tags;
    }
}
